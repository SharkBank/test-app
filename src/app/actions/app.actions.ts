import { Action } from '@ngrx/store';
import { Movies } from '../models/app.model';

export const ADD_TUTORIAL       = '[MOVIES] Add';
export const EMPTY_LIST         = '[MOVIES] Clean';

export class AddTutorial implements Action {
  readonly type = ADD_TUTORIAL;

  constructor(public movies: Movies) {}
}

export class EmptyList implements Action {
  readonly type = EMPTY_LIST;
}

export type Actions =
  AddTutorial |
  EmptyList;
