import {Component, OnInit} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {Http} from '@angular/http';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import * as TutorialActions from './actions/app.actions';
import {Movies} from './models/app.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', './styles/global.scss']
})

export class AppComponent implements OnInit {

  loading: boolean = false;
  showButton: boolean = false;
  errorMessage: boolean;
  defaultImageUrl: string = 'https://i.pinimg.com/564x/c0/79/9f/c0799f64e2ff86383ec33f30f1fabd58.jpg';
  baseUrl: string = 'http://www.omdbapi.com/?apikey=f79aeba3&s=';
  pageNumber: string = '&page=';
  searchValue: string;
  errorText: string;
  page: number = 1;
  movies: Movies[] = [];
  searchTextChanged = new Subject<string>();
  subscription: Subscription;
  isIEOrEdge: boolean;

  constructor(private http: Http, private store: Store<Movies[]>) {
    store.subscribe(res => this.movies = res['movies']);
  }

  ngOnInit() {

    this.isIEOrEdge = /msie\s|trident\/|edge\//i.test(window.navigator.userAgent);

    this.searchValue = 'James Bond';
    this.addMore();

    this.subscription = this.searchTextChanged.pipe(
      debounceTime(200),
      distinctUntilChanged())
      .subscribe(res => {
        this.store.dispatch(new TutorialActions.EmptyList());
        this.searchValue = res.toString();
        this.page = 1;
        this.addMore();
        this.loading = false;
      });
  }

  onKeyUp($event) {
    this.showButton = false;
    this.errorMessage = false;
    if($event.target.value) {
      this.searchTextChanged.next($event.target.value);
      this.loading = true;
    }
  }

  addMore() {

    this.searchEntries(this.searchValue, this.page).subscribe(results => {
      const error = results['Error'];
      const result = results.Search;
      if (error) {
        this.errorText = error;
        this.errorMessage = true;
      } else {
        this.showButton = !(result.length < 10);
        this.store.dispatch(new TutorialActions.AddTutorial(result));
        this.page = this.page + 1;
      }
    });
  }

  searchEntries(term, page) {
    return this.http
      .get(this.baseUrl + term + this.pageNumber + page)
      .pipe(map(res => res.json()));
  }

}
