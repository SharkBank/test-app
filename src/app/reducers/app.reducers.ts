import * as TutorialActions from '../actions/app.actions';
import {Movies} from '../models/app.model';

const initialState: any = [];

export function reducer(state: Movies[] = initialState, action: TutorialActions.Actions) {
  switch (action.type) {
    case TutorialActions.ADD_TUTORIAL:
      return state.concat(action.movies);

    case TutorialActions.EMPTY_LIST:
      return [];

    default:
      return state;
  }
}
