import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {map} from 'rxjs/operators';
import {Http} from '@angular/http';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {

  loading: boolean = true;
  name: any;
  openModal: boolean;
  baseUrl: string = 'http://www.omdbapi.com/?apikey=f79aeba3&i=';
  currentMovieElement: any;
  defaultImageUrl: string = 'https://i.pinimg.com/564x/c0/79/9f/c0799f64e2ff86383ec33f30f1fabd58.jpg';

  @Output() public click: EventEmitter<MouseEvent> = new EventEmitter();

  constructor(private route: ActivatedRoute,
              private router: Router,
              private _location: Location,
              private http: Http) {

    router.events.subscribe((val) => {
      this.openModal = true;
      this.loading = true;
      this.name = this.route.snapshot.paramMap.get('name');
    });

  }

  onButtonClick(event: MouseEvent) {
    event.stopPropagation();
    this.click.emit(event);
  }

  onCloseModal() {
    this.router.navigateByUrl('/');
    this.openModal = false;
  }

  fetchData(term) {
    return this.http
      .get(this.baseUrl + term)
      .pipe(map(res => res.json()));
  }

  ngOnInit() {
    this.fetchData(this.name).subscribe(res => {
      this.currentMovieElement = res;
      this.loading = false;
    });
  }

}
