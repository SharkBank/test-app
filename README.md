### A brief overview of this small application

First of all, I hope that the mistakes I have made are learnable. 
Since I have previously mainly used ReactJS, then there may be errors due to specifics.
Also, the file structure may not be the best.

### Build
I generated angular project as they say in the documentation and not made custom webpack settings. 

### Tests
Did not made test, because I haven't done much testing, so this part is missing.

###  Style
Whole style made without any libraries like bootstrap or materialUI. 
These should help in some cases but I decided do not use for this task.

### Logic 
Logic is as simple as there are main page where we have movies/games etc.. and 
if we navigate then we open new route as modal: 
Made modal because of this application specificity. 
In my opinion it is more comfortable not to open every time new page to see such a small amount of information.
And in this way we can handle more data faster and more logically.
BUT IT ALL DEPENDS ON THE SIZE AND NEEDS OF THE APPLICATION


### To run test-app: 

     1) npm install / yarn install 
     2) ng serve

